# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased 2.0.0 - 201y-mm-dd]
### Added
- [Quartz](http://www.quartz-scheduler.org) to schedule Jobs
- [Flyway](https://flywaydb.org/) for database setup and migration
- [Webjars](http://webjars.org)
- README.md
- CHANGELOG.md

### Changed
- Codebase re-written
- Database re-designed
- Use jsf2 [templates](http://docs.oracle.com/javaee/6/tutorial/doc/giqxp.html) and 
[composite components](http://docs.oracle.com/javaee/6/tutorial/doc/giqzr.html) consistently

### Deprecated
- n/a

### Removed
- [samply.common.upgrade](https://code.mitro.dkfz.de/projects/COM/repos/samply.common.upgrade/browse)
    due to the change to Flyway
- Job/Task management via custom [Future Tasks](http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Future.html)
- necessity for config file

### Fixed
- n/a

### Security
- n/a