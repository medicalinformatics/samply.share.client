/*
 * This file is generated by jOOQ.
*/
package de.samply.share.client.model.db;


import de.samply.share.client.model.db.tables.Broker;
import de.samply.share.client.model.db.tables.Configuration;
import de.samply.share.client.model.db.tables.ConfigurationTimings;
import de.samply.share.client.model.db.tables.Contact;
import de.samply.share.client.model.db.tables.Credentials;
import de.samply.share.client.model.db.tables.Document;
import de.samply.share.client.model.db.tables.EventLog;
import de.samply.share.client.model.db.tables.Inquiry;
import de.samply.share.client.model.db.tables.InquiryAnswer;
import de.samply.share.client.model.db.tables.InquiryDetails;
import de.samply.share.client.model.db.tables.InquiryHandlingRule;
import de.samply.share.client.model.db.tables.InquiryRequestedEntity;
import de.samply.share.client.model.db.tables.InquiryResult;
import de.samply.share.client.model.db.tables.InquiryResultStats;
import de.samply.share.client.model.db.tables.JobSchedule;
import de.samply.share.client.model.db.tables.RequestedEntity;
import de.samply.share.client.model.db.tables.SchemaVersion;
import de.samply.share.client.model.db.tables.Token;
import de.samply.share.client.model.db.tables.Upload;
import de.samply.share.client.model.db.tables.User;
import de.samply.share.client.model.db.tables.UserNotification;
import de.samply.share.client.model.db.tables.UserSeenInquiry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Sequence;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Samply extends SchemaImpl {

    private static final long serialVersionUID = 1424829138;

    /**
     * The reference instance of <code>samply</code>
     */
    public static final Samply SAMPLY = new Samply();

    /**
     * The table <code>samply.broker</code>.
     */
    public final Broker BROKER = de.samply.share.client.model.db.tables.Broker.BROKER;

    /**
     * The table <code>samply.configuration</code>.
     */
    public final Configuration CONFIGURATION = de.samply.share.client.model.db.tables.Configuration.CONFIGURATION;

    /**
     * Use an extra table for timing parameters for the sake of simply using integers
     */
    public final ConfigurationTimings CONFIGURATION_TIMINGS = de.samply.share.client.model.db.tables.ConfigurationTimings.CONFIGURATION_TIMINGS;

    /**
     * The table <code>samply.contact</code>.
     */
    public final Contact CONTACT = de.samply.share.client.model.db.tables.Contact.CONTACT;

    /**
     * Stored credentials for central search,  searchbrokers, local datamanagement...
     */
    public final Credentials CREDENTIALS = de.samply.share.client.model.db.tables.Credentials.CREDENTIALS;

    /**
     * Users can upload (modified) export files in order to track what they may have sent to other people
     */
    public final Document DOCUMENT = de.samply.share.client.model.db.tables.Document.DOCUMENT;

    /**
     * Log certain events that happen during job execution. E.g. Upload was triggered or inquiry was received/executed
     */
    public final EventLog EVENT_LOG = de.samply.share.client.model.db.tables.EventLog.EVENT_LOG;

    /**
     * The table <code>samply.inquiry</code>.
     */
    public final Inquiry INQUIRY = de.samply.share.client.model.db.tables.Inquiry.INQUIRY;

    /**
     * The table <code>samply.inquiry_answer</code>.
     */
    public final InquiryAnswer INQUIRY_ANSWER = de.samply.share.client.model.db.tables.InquiryAnswer.INQUIRY_ANSWER;

    /**
     * The table <code>samply.inquiry_details</code>.
     */
    public final InquiryDetails INQUIRY_DETAILS = de.samply.share.client.model.db.tables.InquiryDetails.INQUIRY_DETAILS;

    /**
     * Incoming inquiries may be handled differently, depending on several criteria. Allow to define an "if" part which has to be handled in the application, a link to a specific broker (may be null), a link to a certain result type (may also be null) as well as an action to take (e.g. instantly reply a number)
     */
    public final InquiryHandlingRule INQUIRY_HANDLING_RULE = de.samply.share.client.model.db.tables.InquiryHandlingRule.INQUIRY_HANDLING_RULE;

    /**
     * The table <code>samply.inquiry_requested_entity</code>.
     */
    public final InquiryRequestedEntity INQUIRY_REQUESTED_ENTITY = de.samply.share.client.model.db.tables.InquiryRequestedEntity.INQUIRY_REQUESTED_ENTITY;

    /**
     * The table <code>samply.inquiry_result</code>.
     */
    public final InquiryResult INQUIRY_RESULT = de.samply.share.client.model.db.tables.InquiryResult.INQUIRY_RESULT;

    /**
     * The table <code>samply.inquiry_result_stats</code>.
     */
    public final InquiryResultStats INQUIRY_RESULT_STATS = de.samply.share.client.model.db.tables.InquiryResultStats.INQUIRY_RESULT_STATS;

    /**
     * For regularly executed tasks, set the cron schedule expression here
     */
    public final JobSchedule JOB_SCHEDULE = de.samply.share.client.model.db.tables.JobSchedule.JOB_SCHEDULE;

    /**
     * An inquirer can request different types of entities (biomaterial, clinical data...)
     */
    public final RequestedEntity REQUESTED_ENTITY = de.samply.share.client.model.db.tables.RequestedEntity.REQUESTED_ENTITY;

    /**
     * The table <code>samply.schema_version</code>.
     */
    public final SchemaVersion SCHEMA_VERSION = de.samply.share.client.model.db.tables.SchemaVersion.SCHEMA_VERSION;

    /**
     * The table <code>samply.token</code>.
     */
    public final Token TOKEN = de.samply.share.client.model.db.tables.Token.TOKEN;

    /**
     * Uploads to central search.
     */
    public final Upload UPLOAD = de.samply.share.client.model.db.tables.Upload.UPLOAD;

    /**
     * The table <code>samply.user</code>.
     */
    public final User USER = de.samply.share.client.model.db.tables.User.USER;

    /**
     * Which user shall receive which notifications?
     */
    public final UserNotification USER_NOTIFICATION = de.samply.share.client.model.db.tables.UserNotification.USER_NOTIFICATION;

    /**
     * The table <code>samply.user_seen_inquiry</code>.
     */
    public final UserSeenInquiry USER_SEEN_INQUIRY = de.samply.share.client.model.db.tables.UserSeenInquiry.USER_SEEN_INQUIRY;

    /**
     * No further instances allowed
     */
    private Samply() {
        super("samply", null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Sequence<?>> getSequences() {
        List result = new ArrayList();
        result.addAll(getSequences0());
        return result;
    }

    private final List<Sequence<?>> getSequences0() {
        return Arrays.<Sequence<?>>asList(
            Sequences.BROKER_ID_SEQ,
            Sequences.CONTACT_ID_SEQ,
            Sequences.CREDENTIALS_ID_SEQ,
            Sequences.DOCUMENT_ID_SEQ,
            Sequences.EVENT_LOG_ID_SEQ,
            Sequences.INQUIRY_ANSWER_ID_SEQ,
            Sequences.INQUIRY_DETAILS_ID_SEQ,
            Sequences.INQUIRY_HANDLING_RULE_ID_SEQ,
            Sequences.INQUIRY_ID_SEQ,
            Sequences.INQUIRY_RESULT_ID_SEQ,
            Sequences.INQUIRY_RESULT_STATS_ID_SEQ,
            Sequences.JOB_SCHEDULE_ID_SEQ,
            Sequences.REQUESTED_ENTITY_ID_SEQ,
            Sequences.TOKEN_ID_SEQ,
            Sequences.UPLOAD_ID_SEQ,
            Sequences.USER_ID_SEQ);
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Broker.BROKER,
            Configuration.CONFIGURATION,
            ConfigurationTimings.CONFIGURATION_TIMINGS,
            Contact.CONTACT,
            Credentials.CREDENTIALS,
            Document.DOCUMENT,
            EventLog.EVENT_LOG,
            Inquiry.INQUIRY,
            InquiryAnswer.INQUIRY_ANSWER,
            InquiryDetails.INQUIRY_DETAILS,
            InquiryHandlingRule.INQUIRY_HANDLING_RULE,
            InquiryRequestedEntity.INQUIRY_REQUESTED_ENTITY,
            InquiryResult.INQUIRY_RESULT,
            InquiryResultStats.INQUIRY_RESULT_STATS,
            JobSchedule.JOB_SCHEDULE,
            RequestedEntity.REQUESTED_ENTITY,
            SchemaVersion.SCHEMA_VERSION,
            Token.TOKEN,
            Upload.UPLOAD,
            User.USER,
            UserNotification.USER_NOTIFICATION,
            UserSeenInquiry.USER_SEEN_INQUIRY);
    }
}
