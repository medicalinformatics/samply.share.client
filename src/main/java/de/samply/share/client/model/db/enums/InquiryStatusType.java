/*
 * This file is generated by jOOQ.
*/
package de.samply.share.client.model.db.enums;


import de.samply.share.client.model.db.Samply;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum InquiryStatusType implements EnumType {

    IS_NEW("IS_NEW"),

    IS_PROCESSING("IS_PROCESSING"),

    IS_READY("IS_READY"),

    IS_LDM_ERROR("IS_LDM_ERROR"),

    IS_ABANDONED("IS_ABANDONED"),

    IS_ARCHIVED("IS_ARCHIVED");

    private final String literal;

    private InquiryStatusType(String literal) {
        this.literal = literal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return getSchema() == null ? null : getSchema().getCatalog();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Samply.SAMPLY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return "inquiry_status_type";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLiteral() {
        return literal;
    }
}
