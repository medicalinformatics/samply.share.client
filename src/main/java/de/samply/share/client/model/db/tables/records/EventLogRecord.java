/*
 * This file is generated by jOOQ.
*/
package de.samply.share.client.model.db.tables.records;


import de.samply.share.client.model.db.enums.EventMessageType;
import de.samply.share.client.model.db.tables.EventLog;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record9;
import org.jooq.Row9;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * Log certain events that happen during job execution. E.g. Upload was triggered 
 * or inquiry was received/executed
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class EventLogRecord extends UpdatableRecordImpl<EventLogRecord> implements Record9<Integer, EventMessageType, Integer, Integer, Integer, Integer, Timestamp, Boolean, String> {

    private static final long serialVersionUID = -1507628502;

    /**
     * Setter for <code>samply.event_log.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>samply.event_log.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>samply.event_log.event_type</code>.
     */
    public void setEventType(EventMessageType value) {
        set(1, value);
    }

    /**
     * Getter for <code>samply.event_log.event_type</code>.
     */
    public EventMessageType getEventType() {
        return (EventMessageType) get(1);
    }

    /**
     * Setter for <code>samply.event_log.inquiry_id</code>.
     */
    public void setInquiryId(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>samply.event_log.inquiry_id</code>.
     */
    public Integer getInquiryId() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>samply.event_log.upload_id</code>.
     */
    public void setUploadId(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>samply.event_log.upload_id</code>.
     */
    public Integer getUploadId() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>samply.event_log.user_id</code>.
     */
    public void setUserId(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>samply.event_log.user_id</code>.
     */
    public Integer getUserId() {
        return (Integer) get(4);
    }

    /**
     * Setter for <code>samply.event_log.quality_report_id</code>.
     */
    public void setQualityReportId(Integer value) {
        set(5, value);
    }

    /**
     * Getter for <code>samply.event_log.quality_report_id</code>.
     */
    public Integer getQualityReportId() {
        return (Integer) get(5);
    }

    /**
     * Setter for <code>samply.event_log.event_time</code>. when did the logged event occur?
     */
    public void setEventTime(Timestamp value) {
        set(6, value);
    }

    /**
     * Getter for <code>samply.event_log.event_time</code>. when did the logged event occur?
     */
    public Timestamp getEventTime() {
        return (Timestamp) get(6);
    }

    /**
     * Setter for <code>samply.event_log.show_in_global</code>.
     */
    public void setShowInGlobal(Boolean value) {
        set(7, value);
    }

    /**
     * Getter for <code>samply.event_log.show_in_global</code>.
     */
    public Boolean getShowInGlobal() {
        return (Boolean) get(7);
    }

    /**
     * Setter for <code>samply.event_log.entry</code>. Either a message or a set of parameters. As a JSON String
     */
    public void setEntry(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>samply.event_log.entry</code>. Either a message or a set of parameters. As a JSON String
     */
    public String getEntry() {
        return (String) get(8);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record9 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row9<Integer, EventMessageType, Integer, Integer, Integer, Integer, Timestamp, Boolean, String> fieldsRow() {
        return (Row9) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row9<Integer, EventMessageType, Integer, Integer, Integer, Integer, Timestamp, Boolean, String> valuesRow() {
        return (Row9) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return EventLog.EVENT_LOG.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<EventMessageType> field2() {
        return EventLog.EVENT_LOG.EVENT_TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return EventLog.EVENT_LOG.INQUIRY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field4() {
        return EventLog.EVENT_LOG.UPLOAD_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field5() {
        return EventLog.EVENT_LOG.USER_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field6() {
        return EventLog.EVENT_LOG.QUALITY_REPORT_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field7() {
        return EventLog.EVENT_LOG.EVENT_TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Boolean> field8() {
        return EventLog.EVENT_LOG.SHOW_IN_GLOBAL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field9() {
        return EventLog.EVENT_LOG.ENTRY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventMessageType value2() {
        return getEventType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getInquiryId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value4() {
        return getUploadId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value5() {
        return getUserId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value6() {
        return getQualityReportId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value7() {
        return getEventTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean value8() {
        return getShowInGlobal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value9() {
        return getEntry();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value2(EventMessageType value) {
        setEventType(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value3(Integer value) {
        setInquiryId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value4(Integer value) {
        setUploadId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value5(Integer value) {
        setUserId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value6(Integer value) {
        setQualityReportId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value7(Timestamp value) {
        setEventTime(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value8(Boolean value) {
        setShowInGlobal(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord value9(String value) {
        setEntry(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventLogRecord values(Integer value1, EventMessageType value2, Integer value3, Integer value4, Integer value5, Integer value6, Timestamp value7, Boolean value8, String value9) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached EventLogRecord
     */
    public EventLogRecord() {
        super(EventLog.EVENT_LOG);
    }

    /**
     * Create a detached, initialised EventLogRecord
     */
    public EventLogRecord(Integer id, EventMessageType eventType, Integer inquiryId, Integer uploadId, Integer userId, Integer qualityReportId, Timestamp eventTime, Boolean showInGlobal, String entry) {
        super(EventLog.EVENT_LOG);

        set(0, id);
        set(1, eventType);
        set(2, inquiryId);
        set(3, uploadId);
        set(4, userId);
        set(5, qualityReportId);
        set(6, eventTime);
        set(7, showInGlobal);
        set(8, entry);
    }
}
