/*
 * This file is generated by jOOQ.
*/
package de.samply.share.client.model.db.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserSeenInquiry implements Serializable {

    private static final long serialVersionUID = 1207625290;

    private Integer userId;
    private Integer inquiryId;

    public UserSeenInquiry() {}

    public UserSeenInquiry(UserSeenInquiry value) {
        this.userId = value.userId;
        this.inquiryId = value.inquiryId;
    }

    public UserSeenInquiry(
        Integer userId,
        Integer inquiryId
    ) {
        this.userId = userId;
        this.inquiryId = inquiryId;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getInquiryId() {
        return this.inquiryId;
    }

    public void setInquiryId(Integer inquiryId) {
        this.inquiryId = inquiryId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("UserSeenInquiry (");

        sb.append(userId);
        sb.append(", ").append(inquiryId);

        sb.append(")");
        return sb.toString();
    }
}
