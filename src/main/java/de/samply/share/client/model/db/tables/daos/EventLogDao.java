/*
 * This file is generated by jOOQ.
*/
package de.samply.share.client.model.db.tables.daos;


import de.samply.share.client.model.db.enums.EventMessageType;
import de.samply.share.client.model.db.tables.EventLog;
import de.samply.share.client.model.db.tables.records.EventLogRecord;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * Log certain events that happen during job execution. E.g. Upload was triggered 
 * or inquiry was received/executed
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class EventLogDao extends DAOImpl<EventLogRecord, de.samply.share.client.model.db.tables.pojos.EventLog, Integer> {

    /**
     * Create a new EventLogDao without any configuration
     */
    public EventLogDao() {
        super(EventLog.EVENT_LOG, de.samply.share.client.model.db.tables.pojos.EventLog.class);
    }

    /**
     * Create a new EventLogDao with an attached configuration
     */
    public EventLogDao(Configuration configuration) {
        super(EventLog.EVENT_LOG, de.samply.share.client.model.db.tables.pojos.EventLog.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(de.samply.share.client.model.db.tables.pojos.EventLog object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchById(Integer... values) {
        return fetch(EventLog.EVENT_LOG.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public de.samply.share.client.model.db.tables.pojos.EventLog fetchOneById(Integer value) {
        return fetchOne(EventLog.EVENT_LOG.ID, value);
    }

    /**
     * Fetch records that have <code>event_type IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchByEventType(EventMessageType... values) {
        return fetch(EventLog.EVENT_LOG.EVENT_TYPE, values);
    }

    /**
     * Fetch records that have <code>inquiry_id IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchByInquiryId(Integer... values) {
        return fetch(EventLog.EVENT_LOG.INQUIRY_ID, values);
    }

    /**
     * Fetch records that have <code>upload_id IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchByUploadId(Integer... values) {
        return fetch(EventLog.EVENT_LOG.UPLOAD_ID, values);
    }

    /**
     * Fetch records that have <code>user_id IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchByUserId(Integer... values) {
        return fetch(EventLog.EVENT_LOG.USER_ID, values);
    }

    /**
     * Fetch records that have <code>quality_report_id IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchByQualityReportId(Integer... values) {
        return fetch(EventLog.EVENT_LOG.QUALITY_REPORT_ID, values);
    }

    /**
     * Fetch records that have <code>event_time IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchByEventTime(Timestamp... values) {
        return fetch(EventLog.EVENT_LOG.EVENT_TIME, values);
    }

    /**
     * Fetch records that have <code>show_in_global IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchByShowInGlobal(Boolean... values) {
        return fetch(EventLog.EVENT_LOG.SHOW_IN_GLOBAL, values);
    }

    /**
     * Fetch records that have <code>entry IN (values)</code>
     */
    public List<de.samply.share.client.model.db.tables.pojos.EventLog> fetchByEntry(String... values) {
        return fetch(EventLog.EVENT_LOG.ENTRY, values);
    }
}
