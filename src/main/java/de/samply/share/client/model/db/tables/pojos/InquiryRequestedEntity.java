/*
 * This file is generated by jOOQ.
*/
package de.samply.share.client.model.db.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class InquiryRequestedEntity implements Serializable {

    private static final long serialVersionUID = -651710186;

    private Integer inquiryId;
    private Integer requestedEntityId;

    public InquiryRequestedEntity() {}

    public InquiryRequestedEntity(InquiryRequestedEntity value) {
        this.inquiryId = value.inquiryId;
        this.requestedEntityId = value.requestedEntityId;
    }

    public InquiryRequestedEntity(
        Integer inquiryId,
        Integer requestedEntityId
    ) {
        this.inquiryId = inquiryId;
        this.requestedEntityId = requestedEntityId;
    }

    public Integer getInquiryId() {
        return this.inquiryId;
    }

    public void setInquiryId(Integer inquiryId) {
        this.inquiryId = inquiryId;
    }

    public Integer getRequestedEntityId() {
        return this.requestedEntityId;
    }

    public void setRequestedEntityId(Integer requestedEntityId) {
        this.requestedEntityId = requestedEntityId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("InquiryRequestedEntity (");

        sb.append(inquiryId);
        sb.append(", ").append(requestedEntityId);

        sb.append(")");
        return sb.toString();
    }
}
