package de.samply.share.client.quality.report.chainlinks.instances.file;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/

import de.samply.share.client.quality.report.chainlinks.ChainLink;
import de.samply.share.client.quality.report.chainlinks.ChainLinkException;
import de.samply.share.client.quality.report.chainlinks.ChainLinkItem;
import de.samply.share.client.quality.report.file.manager.QualityReportFileManagerException;
import de.samply.share.client.quality.report.file.manager.QualityReportMetadataFileManager;
import de.samply.share.client.quality.report.file.metadata.QualityReportMetadata;

import java.util.Date;

public class MetadataQualityReportFileWriter_ChainLink<I extends ChainLinkItem & FileContext> extends ChainLink<I> {


    private QualityReportMetadataFileManager qualityReportMetadataFileManager;

    public MetadataQualityReportFileWriter_ChainLink(QualityReportMetadataFileManager qualityReportMetadataFileManager) {
        this.qualityReportMetadataFileManager = qualityReportMetadataFileManager;
    }

    @Override
    protected String getChainLinkId() {
        return "Metadata File Writer";
    }

    @Override
    protected I process(I item) throws ChainLinkException {

        try {

            writeMetadataFile(item);
            return item;

        } catch (QualityReportFileManagerException e) {
            throw new ChainLinkException(e);
        }

    }

    private void writeMetadataFile(I item) throws QualityReportFileManagerException {

        String fileId = item.getFileId();
        QualityReportMetadata metadata = createMetadata(item, fileId);

        qualityReportMetadataFileManager.write(metadata, fileId);
    }

    private QualityReportMetadata createMetadata(I item, String fileId){

        QualityReportMetadata qualityReportMetadata = new QualityReportMetadata();
        qualityReportMetadata.setCreationTimestamp(new Date());
        qualityReportMetadata.setFileId(fileId);
        
        return qualityReportMetadata;

    }

}
