package de.samply.share.client.quality.report.file.excel.pattern;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/

import de.samply.common.mdrclient.MdrClient;
import de.samply.share.client.quality.report.centraxx.CentraXxMapper;
import de.samply.share.client.quality.report.dktk.DktkId_MdrId_Converter;
import de.samply.share.client.quality.report.file.excel.cell.reference.FirstRowCellReferenceFactoryForOneSheet;
import de.samply.share.client.quality.report.file.excel.instances.patientids.PatientDktkIdsExcelRowContextFactory;
import de.samply.share.client.quality.report.file.excel.instances.patientids.PatientLocalIdsExcelRowContextFactory;
import de.samply.share.client.quality.report.file.excel.row.context.ExcelRowContextFactory_001;
import de.samply.share.client.quality.report.file.excel.row.factory.ExcelRowFactory;
import de.samply.share.client.quality.report.file.excel.row.factory.ExcelRowFactoryImpl;
import de.samply.share.client.quality.report.file.excel.row.mapper.ExcelRowMapper_001;
import de.samply.share.client.quality.report.file.excel.sheet.ExcelSheetFactory;
import de.samply.share.client.quality.report.file.excel.sheet.ExcelSheetFactoryImpl;
import de.samply.share.client.quality.report.file.excel.sheet.TemplateExcelSheetFactory;
import de.samply.share.client.quality.report.file.excel.sheet.wrapper.ExcelSheetWithAutoFilterFactory;
import de.samply.share.client.quality.report.file.excel.sheet.wrapper.ExcelSheetWithAutoSizeColumnFactory;
import de.samply.share.client.quality.report.file.excel.sheet.wrapper.HighlightMismatchInRed_ExcelSheetFactory_001;
import de.samply.share.client.quality.report.file.excel.workbook.ExcelWorkbookFactory;
import de.samply.share.client.quality.report.file.excel.workbook.ExcelWorkbookFactoryImpl_001;
import de.samply.share.client.quality.report.model.Model;
import de.samply.share.client.quality.report.model.searcher.ModelSearcher;

public class ExcelPattern_001 implements ExcelPattern{

    private Model model;
    private MdrClient mdrClient;
    private CentraXxMapper centraXxMapper;
    private DktkId_MdrId_Converter dktkIdManager;



    public ExcelPattern_001(Model model, MdrClient mdrClient, CentraXxMapper centraXxMapper, DktkId_MdrId_Converter dktkIdManager) {

        this.model = model;
        this.mdrClient = mdrClient;
        this.centraXxMapper = centraXxMapper;
        this.dktkIdManager = dktkIdManager;

    }

    @Override
    public ExcelWorkbookFactory createExcelWorkbookFactory() {

        ExcelRowFactory excelRowFactory = new ExcelRowFactoryImpl();
        ExcelRowContextFactory_001 excelRowContextFactory = createExcelRowContextFactory();
        PatientDktkIdsExcelRowContextFactory patientDktkIdsExcelRowContextFactory = new PatientDktkIdsExcelRowContextFactory();
        PatientLocalIdsExcelRowContextFactory patientLocalIdsExcelRowContextFactory = new PatientLocalIdsExcelRowContextFactory();

        ExcelSheetFactory excelSheetFactory = new ExcelSheetFactoryImpl(excelRowFactory);

        excelSheetFactory = new ExcelSheetWithAutoFilterFactory(excelSheetFactory);
        excelSheetFactory = new HighlightMismatchInRed_ExcelSheetFactory_001(excelSheetFactory);
        excelSheetFactory = new ExcelSheetWithAutoSizeColumnFactory(excelSheetFactory);


        return new ExcelWorkbookFactoryImpl_001(excelSheetFactory, createPatternExcelSheetFactory(), new ModelSearcher(model), dktkIdManager, excelRowContextFactory, patientLocalIdsExcelRowContextFactory, patientDktkIdsExcelRowContextFactory);
    }

    private ExcelRowContextFactory_001 createExcelRowContextFactory (){

        ExcelRowMapper_001 excelRowMapper = createExcelRowMapper ();
        return new ExcelRowContextFactory_001(excelRowMapper);

    }

    private ExcelRowMapper_001 createExcelRowMapper (){

        FirstRowCellReferenceFactoryForOneSheet firstRowCellReferenceFactoryForOneSheet = new FirstRowCellReferenceFactoryForOneSheet(ExcelWorkbookFactoryImpl_001.patientLocalIds_sheetTitle);
        return new ExcelRowMapper_001(model, mdrClient, centraXxMapper, dktkIdManager, firstRowCellReferenceFactoryForOneSheet);

    }

    private TemplateExcelSheetFactory createPatternExcelSheetFactory(){

        return new TemplateExcelSheetFactory();

    }

}
