package de.samply.share.client.quality.report.file.id.path;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/

import de.samply.share.client.quality.report.file.csvline.manager.QualityResultCsvLineManager_001;
import de.samply.share.client.quality.report.file.excel.pattern.ExcelPattern_001;

import de.samply.share.client.quality.report.file.metadata.txtcolumn.MetadataTxtColumnManager_001;

public class IdPathManager_001 extends IdPathManagerImpl<QualityResultCsvLineManager_001, ExcelPattern_001, MetadataTxtColumnManager_001> {

    @Override
    public Class<QualityResultCsvLineManager_001> getQualityResultCsvLineManagerClass() {
        return QualityResultCsvLineManager_001.class;
    }

    @Override
    public Class<ExcelPattern_001> getExcelPatternClass() {
        return ExcelPattern_001.class;
    }

    @Override
    public Class<MetadataTxtColumnManager_001> getMetadataTxtColumnManager() {
        return MetadataTxtColumnManager_001.class;
    }

}
