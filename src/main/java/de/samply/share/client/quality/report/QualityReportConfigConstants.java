package de.samply.share.client.quality.report;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/

public class QualityReportConfigConstants {


    public final static String NAMESPACE = "quality.report.namespace";

    public final static String AUTH_USER_ID= "quality.report.authuserid";

    public final static String AUTH_KEY_ID = "quality.report.authkeyid";

    public final static String AUTH_URL = "quality.report.authurl";

    public final static String AUTH_PRIVATE_KEY_BASE_64 = "quality.report.authPrivateKeyBase64";

    public final static String MAX_ATTEMPTS = "quality.report.maxAttempts";

    public final static String MAX_TIME_TO_WAIT_IN_MILLIS = "quality.report.maxTimeToWaitInMillis";

    public final static String ONLY_STATISTICS_AVAILABLE = "quality.report.onlystatisticsavailable";

    public final static String MDR_LINK_PREFIX = "quality.report.mdrLinkPrefix";

    public final static String MAX_NUMBER_OF_PATIENT_IDS_TO_BE_SHOWN = "quality.report.maxNumberOfPatientIdsToBeShown";

    public final static String CENTRAXX_ATTRIBUTES_FILE = "quality.report.centraxx-attributes-file";

    public final static String CENTRAXX_VALUES_FILE = "quality.report.centraxx-values-file";

    public final static String IGNORED_DATAELEMENTS = "quality.report.ignored-dataelements";

    public final static String DIRECTORY = "quality.report.directory";

    public final static String LOCATION = "quality.report.location";

    public final static String BASIC_FILENAME = "quality.report.basic-filename";

    public final static String SCHEDULER_FORMAT = "quality.report.scheduler.format";

    public final static String SCHEDULER_YEARS = "quality.report.scheduler.years";

    public final static String GROUP_MODUL = "quality.report.scheduler.groups-modul";

    public final static String SCHEDULER_BY_YEAR = "by-year";

    public final static String SCHEDULER_BY_MONTH = "by-month";

    public final static String STATISTICS_FILENAME = "quality.report.statistics.filename";

    public final static String EXCEL_INFO_FILENAME = "quality.report.excel.info.filename";

    public final static String EXCEL_INFO_URL = "quality.report.excel.info.url";


}
