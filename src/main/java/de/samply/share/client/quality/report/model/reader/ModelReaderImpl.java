package de.samply.share.client.quality.report.model.reader;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/


import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Result;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.share.client.model.EnumConfiguration;
import de.samply.share.common.utils.MdrIdDatatype;
import de.samply.share.client.quality.report.MdrIdAndValidations;
import de.samply.share.client.quality.report.model.Model;
import de.samply.share.client.util.db.ConfigurationUtil;
import de.samply.web.mdrFaces.MdrContext;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ModelReaderImpl implements ModelReader {

    private static final String DATAELEMENTGROUP = "dataelementgroup";

    private String mdrGroupMdsK;
    private String mdrGroupMdsB;

    private String languageCode;



    public ModelReaderImpl() {

        languageCode = ConfigurationUtil.getConfigurationElementValue(EnumConfiguration.QUALITY_REPORT_LANGUAGE_CODE);
        mdrGroupMdsK = ConfigurationUtil.getConfigurationElementValue(EnumConfiguration.MDR_GRP_MDSK);
        mdrGroupMdsB = ConfigurationUtil.getConfigurationElementValue(EnumConfiguration.MDR_GRP_MDSB);

    }

    @Override
    public Model getModel () throws ModelReaderException {

        try {

            return getModelWithoutExceptions();

        } catch (ExecutionException | MdrInvalidResponseException | MdrConnectionException e) {
            throw new ModelReaderException(e);
        }
    }

    private Model getModelWithoutExceptions () throws ExecutionException, MdrConnectionException, MdrInvalidResponseException {

        List<MdrIdDatatype> mds_k_ids = new ArrayList<>();
        List<MdrIdDatatype> mds_b_ids = new ArrayList<>();


        MdrClient mdrClient = getMdrClient();

        mds_k_ids = getElementsFromGroupAndSubgroups(mds_k_ids, mdrGroupMdsK, mdrClient);
        mds_b_ids = getElementsFromGroupAndSubgroups(mds_b_ids, mdrGroupMdsB, mdrClient);

        List<MdrIdAndValidations> mds_k_andValidationsList = getMdrIdAndValidationsList(mdrClient, mds_k_ids);
        List<MdrIdAndValidations> mds_b_andValidationsList = getMdrIdAndValidationsList(mdrClient, mds_b_ids);

        mds_k_andValidationsList.addAll(mds_b_andValidationsList);
        Model model = new Model();
        model.setMdrIdAndValidations(mds_k_andValidationsList);

        return model;

    }

    private MdrClient getMdrClient(){

        return MdrContext.getMdrContext().getMdrClient();

    }

    private List<MdrIdDatatype> getElementsFromGroupAndSubgroups(List<MdrIdDatatype> theList, String groupKey, MdrClient mdrClient)
            throws MdrConnectionException, ExecutionException {

        List<Result> result_l = mdrClient.getMembers(groupKey, languageCode);
        for (Result r : result_l) {
            if (r.getType().equalsIgnoreCase(DATAELEMENTGROUP)) {
                theList = getElementsFromGroupAndSubgroups(theList, r.getId(), mdrClient);
            } else {
                theList.add(new MdrIdDatatype(r.getId()));
            }
        }

        return theList;
    }


    private List<MdrIdAndValidations> getMdrIdAndValidationsList(MdrClient mdrClient, List<MdrIdDatatype> mdrIds) throws ExecutionException, MdrConnectionException, MdrInvalidResponseException {

        List<MdrIdAndValidations> mdrIdAndValidationsList = new ArrayList<>();

        for (MdrIdDatatype mdrId : mdrIds){
            Validations validations = getMdrIdAndValidationsList(mdrClient, mdrId.getLatestMdr());
            MdrIdAndValidations mdrIdAndValidations = new MdrIdAndValidations(mdrId, validations);
            mdrIdAndValidationsList.add(mdrIdAndValidations);
        }

        return mdrIdAndValidationsList;
    }

    private Validations getMdrIdAndValidationsList(MdrClient mdrClient, String mdrDataElementId) throws ExecutionException, MdrConnectionException, MdrInvalidResponseException {
        return mdrClient.getDataElementValidations(mdrDataElementId, languageCode);
    }

}
