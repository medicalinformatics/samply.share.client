package de.samply.share.client.quality.report.file.downloader;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/

import java.io.*;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;

public class FileDownloaderImpl implements FileDownloader{

    private String sourceUrl;
    private String destinationFilePath;
    private Proxy proxy;


    public FileDownloaderImpl() {
    }

    public FileDownloaderImpl(String sourceUrl, String destinationFilePath) {
        this.sourceUrl = sourceUrl;
        this.destinationFilePath = destinationFilePath;
    }

    @Override
    public void download() throws FileDownloaderException {

        try {

            downloadWithoutExceptionManagement();

        } catch (IOException e) {
            throw new FileDownloaderException(e);
        }

    }

    private void downloadWithoutExceptionManagement() throws IOException {

        URL sourceUrl = new URL (this.sourceUrl);
        URLConnection urlConnection = openConnection(sourceUrl);
        download(urlConnection);

    }

    private URLConnection openConnection (URL url) throws IOException {
        return (proxy != null) ? url.openConnection(proxy) : url.openConnection();
    }

    private void download (URLConnection urlConnection) throws IOException {

        try(InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream())){
            download(inputStream);
        }

    }

    private void download (InputStream inputStream) throws IOException {

        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(destinationFilePath))){

            byte[] chunk = new byte[1024];
            int chunkSize;
            while ((chunkSize = inputStream.read(chunk)) != -1){
                outputStream.write(chunk, 0, chunkSize);
            }

            outputStream.flush();

        }

    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public void setDestinationFilePath(String destinationFilePath) {
        this.destinationFilePath = destinationFilePath;
    }

    @Override
    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }



}
