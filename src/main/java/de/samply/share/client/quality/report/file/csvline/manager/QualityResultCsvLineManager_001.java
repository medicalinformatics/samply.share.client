package de.samply.share.client.quality.report.file.csvline.manager;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/

import de.samply.share.common.utils.MdrIdDatatype;
import de.samply.share.client.quality.report.file.csvline.CsvLine_001;
import de.samply.share.client.quality.report.results.QualityResult;
import de.samply.share.client.quality.report.results.QualityResults;

public class QualityResultCsvLineManager_001 implements QualityResultCsvLineManager {


    @Override
    public String createLine(MdrIdDatatype mdrId, String value, QualityResult qualityResult) {

        CsvLine_001 csvLine = new CsvLine_001();

        csvLine.setMdrId(mdrId);
        csvLine.setAttributeValue(value);
        csvLine.setValid(qualityResult.isValid());
        csvLine.setNumberOfPatients(qualityResult.getNumberOfPatients());

        return csvLine.createLine();

    }

    @Override
    public QualityResults parseLineAndAddToQualityResults(String line, QualityResults qualityResults) {

        CsvLine_001 csvLine = new CsvLine_001();

        csvLine.parseValuesOfLine(line);

        MdrIdDatatype mdrId = csvLine.getMdrId();
        String attributeValue = csvLine.getAttributeValue();
        Integer numberOfPatients = csvLine.getNumberOfPatients();
        Boolean valid = csvLine.isValid();

        if (mdrId != null && attributeValue != null && valid != null && numberOfPatients != null){

            QualityResult qualityResult = new QualityResult();

            qualityResult.setValid(valid);
            qualityResult.setNumberOfPatients(numberOfPatients);

            qualityResults.put(mdrId, attributeValue, qualityResult);

        }

        return qualityResults;

    }

}
