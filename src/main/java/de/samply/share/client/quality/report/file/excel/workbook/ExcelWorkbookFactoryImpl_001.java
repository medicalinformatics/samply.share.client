package de.samply.share.client.quality.report.file.excel.workbook;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/


import de.samply.share.client.quality.report.dktk.DktkId_MdrId_Converter;
import de.samply.share.client.quality.report.file.excel.instances.patientids.PatientDktkIdsExcelRowContextFactory;
import de.samply.share.client.quality.report.file.excel.instances.patientids.PatientLocalIdsExcelRowContextFactory;
import de.samply.share.client.quality.report.file.excel.row.context.ExcelRowContext;
import de.samply.share.client.quality.report.file.excel.row.context.ExcelRowContextFactory_001;
import de.samply.share.client.quality.report.file.excel.sheet.ExcelSheetFactory;
import de.samply.share.client.quality.report.file.excel.sheet.ExcelSheetFactoryException;
import de.samply.share.client.quality.report.file.excel.sheet.TemplateExcelSheetFactory;
import de.samply.share.client.quality.report.model.searcher.ModelSearcher;
import de.samply.share.client.quality.report.results.QualityResults;
import de.samply.share.client.quality.report.results.filter.QualityResultsSortedMdrIdsByDktkIdFilter;
import de.samply.share.client.quality.report.results.filter.QualityResultsValidDateFilter;
import de.samply.share.client.quality.report.results.filter.QualityResultsValidIntegerFilter;
import de.samply.share.client.quality.report.results.filter.QualityResultsValidStringFilter;
import de.samply.share.client.quality.report.results.sorted.AlphabeticallySortedMismatchedQualityResults;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWorkbookFactoryImpl_001 implements ExcelWorkbookFactory {


    private ExcelRowContextFactory_001 excelRowContextFactory;
    private PatientLocalIdsExcelRowContextFactory patientLocalIdsExcelRowContextFactory;
    private PatientDktkIdsExcelRowContextFactory patientDktkIdsExcelRowContextFactory;


    private ExcelSheetFactory excelSheetFactory;
    private TemplateExcelSheetFactory templateExcelSheetFactory;
    private ModelSearcher modelSearcher;
    private DktkId_MdrId_Converter dktkIdManager;

    public final static String allElements_sheetTitle = "all elements";
    public final static String filteredElements_sheetTitle = "filtered elements";
    public final static String patientLocalIds_sheetTitle = "patient local ids";
    public final static String patientDktkIds_sheetTitle = "patient dktk ids";

    protected static final Logger logger = LogManager.getLogger(ExcelWorkbookFactoryImpl_001.class);



    public ExcelWorkbookFactoryImpl_001(ExcelSheetFactory excelSheetFactory, ModelSearcher modelSearcher, DktkId_MdrId_Converter dktkIdManager, ExcelRowContextFactory_001 excelRowContextFactory, PatientLocalIdsExcelRowContextFactory patientLocalIdsExcelRowContextFactory, PatientDktkIdsExcelRowContextFactory patientDktkIdsExcelRowContextFactory) {
        this (excelSheetFactory, null, modelSearcher, dktkIdManager, excelRowContextFactory, patientLocalIdsExcelRowContextFactory, patientDktkIdsExcelRowContextFactory);
    }

    public ExcelWorkbookFactoryImpl_001(ExcelSheetFactory excelSheetFactory, TemplateExcelSheetFactory patternExcelSheetFactory, ModelSearcher modelSearcher, DktkId_MdrId_Converter dktkIdManager, ExcelRowContextFactory_001 excelRowContextFactory, PatientLocalIdsExcelRowContextFactory patientLocalIdsExcelRowContextFactory, PatientDktkIdsExcelRowContextFactory patientDktkIdsExcelRowContextFactory) {

        this.excelSheetFactory = excelSheetFactory;
        this.patientLocalIdsExcelRowContextFactory = patientLocalIdsExcelRowContextFactory;
        this.patientDktkIdsExcelRowContextFactory = patientDktkIdsExcelRowContextFactory;

        this.templateExcelSheetFactory = patternExcelSheetFactory;
        this.modelSearcher = modelSearcher;

        this.dktkIdManager = dktkIdManager;
        this.excelRowContextFactory = excelRowContextFactory;

    }

    @Override
    public XSSFWorkbook createWorkbook(QualityResults qualityResults) throws ExcelWorkbookFactoryException {

        XSSFWorkbook workbook = new XSSFWorkbook();

        QualityResults filteredQualityResults = applyFiltersToQualityResults(qualityResults);
        AlphabeticallySortedMismatchedQualityResults asmQualityResults = new AlphabeticallySortedMismatchedQualityResults(qualityResults);

        logger.info("Adding main sheet to Excel quality report file");
        if (templateExcelSheetFactory != null){
            XSSFWorkbook workbook2 = addTemplateSheet(workbook);
            if (workbook2 != null){
                workbook = workbook2;
            }
        }

        logger.info("Adding filtered elements to quality report file");
        workbook = addSheet(workbook, filteredElements_sheetTitle, filteredQualityResults, asmQualityResults);

        logger.info("Adding all elements to quality report file");
        workbook = addSheet(workbook, allElements_sheetTitle, qualityResults, asmQualityResults);

        logger.info("Adding mismatching patient local ids");
        workbook = addPatientLocalIdsSheet(workbook, asmQualityResults);

        return workbook;

    }

    private XSSFWorkbook addSheet (XSSFWorkbook workbook, String sheetTitle, QualityResults qualityResults) throws ExcelWorkbookFactoryException {

        ExcelRowContext excelRowContext = createExcelRowContext(qualityResults);
        return addSheet(workbook, sheetTitle, excelRowContext);

    }

    private XSSFWorkbook addSheet (XSSFWorkbook workbook, String sheetTitle, QualityResults qualityResults, AlphabeticallySortedMismatchedQualityResults asmQualityResults) throws ExcelWorkbookFactoryException {

        ExcelRowContext excelRowContext = createExcelRowContext(qualityResults, asmQualityResults);
        return addSheet(workbook, sheetTitle, excelRowContext);

    }

    private XSSFWorkbook addPatientLocalIdsSheet (XSSFWorkbook workbook, AlphabeticallySortedMismatchedQualityResults qualityResults) throws ExcelWorkbookFactoryException {

        ExcelRowContext excelRowContext = patientLocalIdsExcelRowContextFactory.createExcelRowContext(qualityResults);
        return addSheet(workbook, patientLocalIds_sheetTitle, excelRowContext);

    }

    private XSSFWorkbook addPatientDktkIdsSheet (XSSFWorkbook workbook, AlphabeticallySortedMismatchedQualityResults qualityResults) throws ExcelWorkbookFactoryException {

        ExcelRowContext excelRowContext = patientDktkIdsExcelRowContextFactory.createExcelRowContext(qualityResults);
        return addSheet(workbook, patientDktkIds_sheetTitle, excelRowContext);

    }

    private XSSFWorkbook addSheet (XSSFWorkbook workbook, String sheetTitle, ExcelRowContext excelRowContext) throws ExcelWorkbookFactoryException {

        try {

            return excelSheetFactory.addSheet(workbook, sheetTitle, excelRowContext);

        } catch (ExcelSheetFactoryException e) {
            throw new ExcelWorkbookFactoryException(e);
        }

    }

    private ExcelRowContext createExcelRowContext (QualityResults qualityResults){
        return excelRowContextFactory.createExcelRowContext(qualityResults);
    }

    private ExcelRowContext createExcelRowContext (QualityResults qualityResults, AlphabeticallySortedMismatchedQualityResults asmQualityResults){
        return excelRowContextFactory.createExcelRowContext(qualityResults, asmQualityResults);
    }

    private XSSFWorkbook addTemplateSheet(XSSFWorkbook workbook) throws ExcelWorkbookFactoryException {

        try {
            return templateExcelSheetFactory.addSheet(workbook, null, null);
        } catch (ExcelSheetFactoryException e) {
            throw new ExcelWorkbookFactoryException(e);
        }

    }

    private QualityResults applyFiltersToQualityResults (QualityResults qualityResults){

        qualityResults = new QualityResultsValidDateFilter(qualityResults, modelSearcher);
        qualityResults = new QualityResultsValidIntegerFilter(qualityResults, modelSearcher);
        qualityResults = new QualityResultsValidStringFilter(qualityResults, modelSearcher);
        qualityResults = new QualityResultsSortedMdrIdsByDktkIdFilter(qualityResults, dktkIdManager);

        return qualityResults;

    }


}
