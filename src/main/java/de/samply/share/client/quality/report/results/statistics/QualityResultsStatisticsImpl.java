package de.samply.share.client.quality.report.results.statistics;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/

import de.samply.share.common.utils.MdrIdDatatype;
import de.samply.share.client.quality.report.results.QualityResults;

public class QualityResultsStatisticsImpl implements QualityResultsStatistics {

    private QualityResults qualityResults;

    public QualityResultsStatisticsImpl(QualityResults qualityResults) {
        this.qualityResults = qualityResults;
    }

    private double getPercentage (int part, int total){
        return ((double) part) / ((double) total);
    }

    @Override
    public double getPercentageOf_PatientsWithValue_outOf_PatientsWithMdrId(MdrIdDatatype mdrId, String value) {

        int patientsWithValue = 0;
        int patientsWithMdrId = 0;

        //TODO
        return getPercentage(patientsWithValue, patientsWithMdrId);

    }

    @Override
    public double getPercentageOf_MismatchingPatientsWithValue_outOf_MismatchingPatientsWithMdrId(MdrIdDatatype mdrId, String value) {

        int mismatchingPatientsWithValue = 0;
        int mismatchingPatientsWithMdrId = 0;

        //TODO
        return getPercentage(mismatchingPatientsWithValue, mismatchingPatientsWithMdrId);

    }

    @Override
    public double getPercentageOf_MismatchingPatientsWithMdrId_outOf_PatientsWithMdrId(MdrIdDatatype mdrId) {

        int mismatchingPatientsWithMdrId = 0;
        int patientsWithMdrId = 0;

        //TODO
        return getPercentage(mismatchingPatientsWithMdrId, patientsWithMdrId);

    }

    @Override
    public double getPercentageOf_MatchingPatientsWithMdrId_outOf_PatientsWithMdrId(MdrIdDatatype mdrId) {

        int matchingPatientsWithMdrId = 0;
        int patientsWithMdrId = 0;


        //TODO
        return getPercentage(matchingPatientsWithMdrId, patientsWithMdrId);

    }

    @Override
    public int getNumberOf_MismatchingPatientsWithMdrId(MdrIdDatatype mdrId) {

        int mismatchingPatientsWithMdrId = 0;

        //TODO
        return 0;

    }

    @Override
    public int getNumberOf_MatchingPatientsWithMdrId(MdrIdDatatype mdrId) {

        int MatchingPatientsWithMdrId = 0;

        //TODO
        return 0;

    }

    @Override
    public int getNumberOf_PatientsForValidation(MdrIdDatatype mdrId) {

        int patientsForValidation = 0;

        //TODO
        return 0;

    }

    @Override
    public int getNumberOf_PatientsForId(MdrIdDatatype mdrId) {

        int patientsForId = 0;

        //TODO
        return 0;

    }

    @Override
    public double getPercentage_OfMatchingDataelements_outOf_AllDataelements() {

        int matchingDataelements = 0;

        //TODO
        return 0;

    }

    @Override
    public double getPercentage_OfNotCompletelyMismatchingDataelements_outOf_AllDataelements() {

        int notCompletelyMismatchingDataelements = 0;
        int allDataelements = 0;

        //TODO
        return 0;

    }

    @Override
    public double getPercentage_OfCompletelyMismatchingDataelements_outOf_AllDataelements() {

        int completelyMismatchingDataelements = 0;
        int allDataelements = 0;

        //TODO
        return 0;

    }

    @Override
    public double getPercentage_OfNonExistingDataelements_outOf_AllDataelements() {

        int nonExistingDataelements = 0;
        int allDataelements = 0;

        //TODO
        return 0;

    }

}
