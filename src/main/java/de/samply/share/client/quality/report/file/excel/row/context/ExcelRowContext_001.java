package de.samply.share.client.quality.report.file.excel.row.context;/*
* Copyright (C) 2017 Medizinische Informatik in der Translationalen Onkologie,
* Deutsches Krebsforschungszentrum in Heidelberg
*
* This program is free software; you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the Free
* Software Foundation; either version 3 of the License, or (at your option) any
* later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU Affero General Public License
* along with this program; if not, see http://www.gnu.org/licenses.
*
* Additional permission under GNU GPL version 3 section 7:
*
* If you modify this Program, or any covered work, by linking or combining it
* with Jersey (https://jersey.java.net) (or a modified version of that
* library), containing parts covered by the terms of the General Public
* License, version 2.0, the licensors of this Program grant you additional
* permission to convey the resulting work.
*/

import de.samply.share.common.utils.MdrIdDatatype;
import de.samply.share.client.quality.report.file.excel.row.elements.ExcelRowElements;
import de.samply.share.client.quality.report.file.excel.row.elements.ExcelRowElements_001;
import de.samply.share.client.quality.report.file.excel.row.mapper.ExcelRowMapperException;
import de.samply.share.client.quality.report.file.excel.row.mapper.ExcelRowMapper_001;
import de.samply.share.client.quality.report.results.QualityResult;
import de.samply.share.client.quality.report.results.QualityResults;
import de.samply.share.client.quality.report.results.sorted.AlphabeticallySortedMismatchedQualityResults;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelRowContext_001 implements ExcelRowContext{

    protected static final Logger logger = LogManager.getLogger(ExcelRowContext_001.class);

    private List<ExcelRowParameters_001> excelRowParametersList = new ArrayList<>();
    private ExcelRowMapper_001 excelRowMapper;
    private AlphabeticallySortedMismatchedQualityResults asmQualityResults;

    public ExcelRowContext_001(ExcelRowMapper_001 excelRowMapper, QualityResults qualityResults) {
        this (excelRowMapper, qualityResults, null);
    }

    public ExcelRowContext_001(ExcelRowMapper_001 excelRowMapper, QualityResults qualityResults, AlphabeticallySortedMismatchedQualityResults asmQualityResults) {

        this.excelRowMapper = excelRowMapper;
        this.asmQualityResults = asmQualityResults;
        fillOutExcelRowParametersList(qualityResults);

    }

    private void fillOutExcelRowParametersList (QualityResults qualityResults){

        for (MdrIdDatatype mdrId : qualityResults.getMdrIds()){

            for (String value : qualityResults.getValues(mdrId)){

                QualityResult qualityResult = qualityResults.getResult(mdrId, value);

                ExcelRowParameters_001 excelRowParameters = createRowParameters(mdrId, value, qualityResult);
                excelRowParametersList.add(excelRowParameters);

            }

        }

    }

    private ExcelRowParameters_001 createRowParameters (MdrIdDatatype mdrId, String value, QualityResult qualityResult){

        ExcelRowParameters_001 rowParameters = new ExcelRowParameters_001();
        Integer mismatchOrdinal = getMismatchOrdinal(mdrId, value);

        rowParameters.setMdrId(mdrId);
        rowParameters.setValue(value);
        rowParameters.setQualityResult(qualityResult);
        rowParameters.setMismatchOrdinal(mismatchOrdinal);

        return rowParameters;

    }

    private Integer getMismatchOrdinal (MdrIdDatatype mdrId, String value){

        if (asmQualityResults == null) return null;
        Integer ordinal = asmQualityResults.getOrdinal(mdrId, value);
        return (ordinal < 0) ? null : ordinal;

    }

    @Override
    public ExcelRowElements createEmptyExcelRowElements() {
        return new ExcelRowElements_001();
    }

    private class ExcelRowContextIterator implements Iterator<ExcelRowElements>{

        private Iterator<ExcelRowParameters_001> excelRowParametersIterator;

        public ExcelRowContextIterator(Iterator<ExcelRowParameters_001> excelRowParametersIterator) {
            this.excelRowParametersIterator = excelRowParametersIterator;
        }

        @Override
        public boolean hasNext() {
            return excelRowParametersIterator.hasNext();
        }

        @Override
        public ExcelRowElements next() {

            ExcelRowParameters_001 next = excelRowParametersIterator.next();
            return convert(next);

        }

        private ExcelRowElements convert (ExcelRowParameters_001 excelRowParameters){
            try {

                return excelRowMapper.createExcelRowElements(excelRowParameters);

            } catch (ExcelRowMapperException e) {

                logger.error(e.getMessage(), e);
                return createEmptyExcelRowElements();// If there is an exception, the exception is logged and the row is left clean in QB.

            }
        }

        @Override
        public void remove() {
            excelRowParametersIterator.remove();
        }

    }



    @Override
    public Iterator<ExcelRowElements> iterator() {
        return new ExcelRowContextIterator(excelRowParametersList.iterator());
    }

}
